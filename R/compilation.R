modules <- function() {
	c("base", "describe", "matfunctions", "matrix", "random", "reg", "vector")
}

moddir <- function() {
	"gretl"
}

# For inline D functions
fpicIncludes <- function() {
	paste0(" -version=gretl -version=r ", paste0(find.package("dmdgretl"), "/", moddir(), "/", modules(), ".d", collapse=" "))
}

# For inline D functions
soFlags <- function() {
	ofiles <- paste0(modules(), ".o", collapse=" ")
	gretlLib <- system("locate -b '\\libgretl-1.0.so' -l 1", intern=TRUE)
	gretlInterface <- paste0(find.package("dmdgretl"), "/libs/libdgretl.so")
	return(paste0(" ", ofiles, " ", gretlLib))
}

# For embedding R inside a D program
flags <- function() {
	lib <- system("locate -b '\\libgretl-1.0.so' -l 1", intern=TRUE)
	return(paste0(" -version=gretl -version=r -L", lib))
}

deps <- function() {
	return(NULL)
}
